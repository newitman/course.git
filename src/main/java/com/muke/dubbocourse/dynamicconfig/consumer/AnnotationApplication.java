package com.muke.dubbocourse.dynamicconfig.consumer;

import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.muke.dubbocourse.common.api.BookFacade;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 动态配置中心 服务消费者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
@EnableDubbo
@EnableNacosConfig // 激活 Nacos 配置
@NacosPropertySource(dataId = "muke")
public class AnnotationApplication {

    static {
        System.setProperty("nacos.server-addr", "127.0.0.1:8848");
    }

    @DubboReference(version = "0.0.1",group = "g1")
    private BookFacade bookFacade;

    public static void main(String[] args) throws IOException {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(AnnotationApplication.class);

        context.refresh();

        AnnotationApplication annotationApplication = context.getBean(AnnotationApplication.class);

        System.out.println("Result=>"+annotationApplication.bookFacade.queryAll());

        System.in.read();

    }

}
