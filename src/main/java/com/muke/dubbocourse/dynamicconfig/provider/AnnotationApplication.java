package com.muke.dubbocourse.dynamicconfig.provider;

import com.alibaba.nacos.api.annotation.NacosProperties;
import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 动态配置中心 提供者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
@EnableDubbo(scanBasePackages = "com.muke.dubbocourse.dynamicconfig")
@EnableNacosConfig(globalProperties = @NacosProperties(serverAddr = "127.0.0.1:8848")) // 激活 Nacos 配置
@NacosPropertySource(dataId = "muke")
public class AnnotationApplication {

    public static void main(String[] args) throws IOException {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(AnnotationApplication.class);

        context.refresh();

        System.in.read();

    }

}
