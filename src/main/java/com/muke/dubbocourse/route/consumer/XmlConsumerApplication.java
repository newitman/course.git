package com.muke.dubbocourse.route.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 条件路由
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlConsumerApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("route/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

//        CompletableFuture.runAsync(()->{
//
//            BookFacade bookFacade = context.getBean(BookFacade.class);
//
//            List<Book> books = bookFacade.queryAll();
//
//            System.out.println("Result1=> "+ books);
//
//        });

        BookFacade bookFacade = context.getBean(BookFacade.class);

        RequestParameter requestParameter = new RequestParameter();

        requestParameter.setName("SpringBoot");

        List<Book> books = bookFacade.queryByName(requestParameter);

        System.out.println("Result2=> "+ books);

        System.in.read();

        //context.close();

    }

}
