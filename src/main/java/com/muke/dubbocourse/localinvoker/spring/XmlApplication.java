package com.muke.dubbocourse.localinvoker.spring;

import com.muke.dubbocourse.common.api.BookFacade;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 本地调用
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("localinvoker/spring/dubbo-xml.xml");

        context.start();

        BookFacade bookFacade = (BookFacade) context.getBean("bookFacade");

        System.out.println("Result=>"+bookFacade.queryAll());

        System.in.read();

    }

}
