package com.muke.dubbocourse.versiongroup.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description多版本与分组
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("versiongroup/consumer/spring/dubbo-consumer-xml.xml");

        BookFacade bookFacade = context.getBean(BookFacade.class);

        System.out.println("Result=>"+bookFacade.queryAll());

        context.start();

        System.in.read();

        //context.close();

    }

}
