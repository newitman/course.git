package com.muke.dubbocourse.lazyandsticky.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.domain.Book;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 延迟和粘滞连接
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("lazyandsticky/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        BookFacade bookFacade = context.getBean(BookFacade.class);

        for ( int i = 0; i < 30; i++) {

           final int index = i;

            new Thread(()->{

                List<Book> books = bookFacade.queryAll();

                System.out.println("The invoker "+index+" result is "+ books);

            }).start();

        }

        System.in.read();

        //context.close();

    }

}
