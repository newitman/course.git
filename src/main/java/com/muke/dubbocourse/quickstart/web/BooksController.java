package com.muke.dubbocourse.quickstart.web;
import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.domain.Book;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BooksController
 * @description 对外提供Http访问
 * @JunitTest: {@link  }
 * @date 2020-10-21 23:12
 **/
@RestController
@RequestMapping("/books")
public class BooksController {


    @DubboReference
    private BookFacade bookFacade;

    @GetMapping("/query")
    public List<Book> queryAll() {

        return bookFacade.queryAll();

    }

}
