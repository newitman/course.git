package com.muke.dubbocourse.quickstart.facade;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.domain.Book;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookFacadeImpl
 * @description 对外提供Book查询服务
 * @JunitTest: {@link  }
 * @date 2020-10-21 23:15
 **/
@DubboService
public class BookFacadeImpl implements BookFacade {

    private static final List<Book> books = new ArrayList<>();

    static {

        Book book1 = new Book();
        book1.setName("SpringBoot");
        book1.setDesc("SpringBoot实战");

        Book book2 = new Book();
        book2.setName("Spring");
        book2.setDesc("Spring实战");

        books.add(book1);

        books.add(book2);


    }

    @Override
    public List<Book> queryAll() {
        return books;
    }
}
