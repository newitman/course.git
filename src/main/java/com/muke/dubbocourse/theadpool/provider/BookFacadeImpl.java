package com.muke.dubbocourse.theadpool.provider;


import cn.hutool.core.util.StrUtil;
import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookFacadeImpl
 * @description
 * @JunitTest: {@link  }
 * @date 2020-10-24 13:31
 **/
public class BookFacadeImpl implements BookFacade {

    Logger logger = LoggerFactory.getLogger(getClass());

    private static final List<Book> books = new ArrayList<>();

    static {

        Book book1 = new Book();
        book1.setName("SpringBoot");
        book1.setDesc("SpringBoot实战");

        Book book2 = new Book();
        book2.setName("Spring");
        book2.setDesc("Spring实战");

        books.add(book1);

        books.add(book2);


    }


    @Override
    public List<Book> queryAll() {

        try {
            logger.info("the queryAll method is invoked !");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return books;
    }

    @Override
    public List<Book> queryByName(RequestParameter request) {

        logger.info("the queryByName method is invoked !");
        List<Book> books = BookFacadeImpl.books.stream()
                .filter(book -> StrUtil.equals(book.getName(), request.getName()))
                .collect(Collectors.toList());

        return books;

    }

}
