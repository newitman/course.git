package com.muke.dubbocourse.generic.consumer;

import com.muke.dubbocourse.common.api.RequestParameter;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 泛化调用 服务调用端
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("generic/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        GenericService bookFacade = (GenericService) context.getBean("bookFacade");

        RequestParameter parameter = new RequestParameter();

        parameter.setName("SpringBoot");

        Object result = bookFacade.$invoke("queryByName", new String[] { "com.muke.dubbocourse.common.api.RequestParameter" }, new Object[] { parameter });

        System.out.println("Result=>"+result);

        System.in.read();

    }

}
