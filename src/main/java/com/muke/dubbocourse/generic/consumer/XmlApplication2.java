package com.muke.dubbocourse.generic.consumer;

import com.muke.dubbocourse.common.api.RequestParameter;
import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 泛化调用 服务调用端 Note：采用Java API方式调用
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication2 {

    public static void main(String[] args) throws IOException {

        // 引用远程服务
        ReferenceConfig<GenericService> reference = new ReferenceConfig<>();
        // 弱类型接口名
        reference.setInterface("com.muke.dubbocourse.common.api.BookFacade");

        //注册中心配置
        RegistryConfig registryConfig = new RegistryConfig();

        registryConfig.setAddress("zookeeper://127.0.0.1:2181");

        reference.setRegistry(registryConfig);

        //应用配置
        ApplicationConfig applicationConfig = new ApplicationConfig();

        applicationConfig.setName("demo-consumer");

        reference.setApplication(applicationConfig);
        // 声明为泛化接口
        reference.setGeneric(true);

        // 用org.apache.dubbo.rpc.service.GenericService可以替代所有接口引用
        GenericService genericService = reference.get();

        RequestParameter parameter = new RequestParameter();

        parameter.setName("SpringBoot");

        Object result = genericService.$invoke("queryByName", new String[]{"com.muke.dubbocourse.common.api.RequestParameter"}, new Object[]{parameter});

        System.out.println("Result=>" + result);

        System.in.read();

    }

}
