package com.muke.dubbocourse.tokenverify.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.domain.Book;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description Dubbo 令牌验证和优雅停机
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlConsumerApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("tokenverify/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        BookFacade bookFacade = context.getBean(BookFacade.class);

        List<Book> books = bookFacade.queryAll();

        System.out.println("Result=> "+ books);

        System.in.read();

        //context.close();

    }

}
