package com.muke.dubbocourse.eventnotify.provider;


import cn.hutool.core.util.StrUtil;
import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookFacadeImpl
 * @description
 * @JunitTest: {@link  }
 * @date 2020-10-24 13:31
 **/
@DubboService(version = "0.0.1")
public class BookFacadeImpl implements BookFacade {

    private static final List<Book> books = new ArrayList<>();

    static {

        Book book1 = new Book();
        book1.setName("SpringBoot");
        book1.setDesc("SpringBoot实战");

        Book book2 = new Book();
        book2.setName("Spring");
        book2.setDesc("Spring实战");

        books.add(book1);

        books.add(book2);


    }

    @Override
    public List<Book> queryAll() {
        return books;
    }

    @Override
    public List<Book> queryByName(RequestParameter request) {
        return books.stream()
                .filter(book -> StrUtil.equals(book.getName(), request.getName()))
                .collect(Collectors.toList());
    }
}
