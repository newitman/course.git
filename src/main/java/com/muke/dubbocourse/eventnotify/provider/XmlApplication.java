package com.muke.dubbocourse.eventnotify.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 事件通知 服务提供者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("eventnotify/provider/spring/dubbo-provider-xml.xml");

        context.start();

        System.in.read();

    }

}
