package com.muke.dubbocourse.eventnotify.consumer;

import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;

import java.util.List;

/**
 *@author <a href="http://youngitman.tech">青年IT男</a>
 *@version v1.0.0
 *@className 通知
 *@description
 *@JunitTest: {@link  }
 *@date 10:07 PM 2020/11/14
 *
**/
public interface Notify {

    void onreturn(List<Book> books, RequestParameter requestParameter);

    void onthrow(Throwable ex, RequestParameter requestParameter);

}
