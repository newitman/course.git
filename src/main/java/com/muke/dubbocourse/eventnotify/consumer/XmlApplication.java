package com.muke.dubbocourse.eventnotify.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.api.RequestParameter;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 事件通知 服务消费者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("eventnotify/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        BookFacade bookFacade = context.getBean(BookFacade.class);

        RequestParameter requestParameter = new RequestParameter();

        requestParameter.setName("SpringBoot");

        System.out.println("Result=>"+bookFacade.queryByName(requestParameter));

        System.in.read();

        //context.close();

    }

}
