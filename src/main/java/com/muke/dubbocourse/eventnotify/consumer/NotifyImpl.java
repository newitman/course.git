package com.muke.dubbocourse.eventnotify.consumer;

import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;

import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className NotifyImpl
 * @description
 * @JunitTest: {@link  }
 * @date 2020-11-14 22:09
 **/
public class NotifyImpl implements Notify{

    @Override
    public void onreturn(List<Book> books, RequestParameter requestParameter) {
        System.out.println("调方法请求参数:" +requestParameter+",返回结果："+books);
    }

    @Override
    public void onthrow(Throwable ex, RequestParameter requestParameter) {
        System.out.println("调用方法抛出异常："+ex+",方法参数："+requestParameter);
    }
}
