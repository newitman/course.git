package com.muke.dubbocourse.spi.custom;

import org.apache.dubbo.rpc.*;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className CustomFilter
 * @description 自定义过滤器
 * @JunitTest: {@link  }
 * @date 2020-12-06 14:28
 **/
public class CustomFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {

        System.out.println("自定义过滤器执行前");

        Result result = invoker.invoke(invocation);

        System.out.println("自定义过滤器执行后");

        return result;
    }
}
