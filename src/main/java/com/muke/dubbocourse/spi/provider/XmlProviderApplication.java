package com.muke.dubbocourse.spi.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description SPI
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlProviderApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spi/provider/spring/dubbo-provider-xml.xml");

        context.start();

        System.in.read();

    }

}
