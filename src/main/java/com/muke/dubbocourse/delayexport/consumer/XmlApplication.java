package com.muke.dubbocourse.delayexport.consumer;

import com.muke.dubbocourse.async.api.BookFacade;
import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 延迟暴露服务消费者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("delayexport/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        //futureInvoke(context);

        rpcContextInvoke(context);

        System.in.read();

        //context.close();

    }

    /**
     *
     * 使用RpcContext
     *
     * @author liyong
     * @date 11:33 PM 2020/11/9
     * @param context
     * @exception
     * @return void
     **/
    private static void rpcContextInvoke(ClassPathXmlApplicationContext context) {

        CompletableFuture<List<Book>> future = RpcContext.getContext().asyncCall(
                () -> {
                    BookFacade bookFacade = context.getBean(BookFacade.class);

                    RequestParameter request = new RequestParameter();

                    request.setName("SpringBoot");

                   return bookFacade.queryByName(request);//返回null
                }
        );

        try {

            System.out.println("Result=>" + future.get());

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    /**
     *
     * 使用CompletableFuture
     *
     * @author liyong
     * @date 11:33 PM 2020/11/9
     * @param context
     * @exception
     * @return void
     **/
    private static void futureInvoke(ClassPathXmlApplicationContext context) {

        BookFacade bookFacade = context.getBean(BookFacade.class);

        CompletableFuture<List<Book>> future = bookFacade.queryAll();

        future.whenComplete((retValue, exception) -> {

            if (exception == null) {

                System.out.println("Result=>" + retValue);

            } else {

                exception.printStackTrace();

            }
        });

    }

}
