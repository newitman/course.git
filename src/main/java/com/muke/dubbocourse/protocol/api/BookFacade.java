package com.muke.dubbocourse.protocol.api;

import com.muke.dubbocourse.common.domain.Book;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookService
 * @description
 * @JunitTest: {@link  }
 * @date 2020-10-24 13:29
 **/
@Path("/books")
public interface BookFacade {

   @GET
   @Path("/queryAll")
   @Consumes({MediaType.APPLICATION_JSON})//指定提交内容类型
   @Produces(MediaType.APPLICATION_JSON)//指定响应类型
   List<Book> queryAll();

}
