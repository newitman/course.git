package com.muke.dubbocourse.async.api;

import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookService
 * @description
 * @JunitTest: {@link  }
 * @date 2020-10-24 13:29
 **/
public interface BookFacade {


    CompletableFuture<List<Book>> queryAll();

    List<Book> queryByName(@NotNull RequestParameter request);

}
