package com.muke.dubbocourse.context.provider;


import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.domain.Book;
import org.apache.dubbo.rpc.RpcContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookFacadeImpl
 * @description
 * @JunitTest: {@link  }
 * @date 2020-10-24 13:31
 **/
public class BookFacadeImpl implements BookFacade {

    private static final List<Book> books = new ArrayList<>();

    static {

        Book book1 = new Book();
        book1.setName("SpringBoot");
        book1.setDesc("SpringBoot实战");

        Book book2 = new Book();
        book2.setName("Spring");
        book2.setDesc("Spring实战");

        books.add(book1);

        books.add(book2);


    }

    @Override
    public List<Book> queryAll() {

        // 本端是否为提供端，这里会返回true
        boolean isProviderSide = RpcContext.getContext().isProviderSide();

        // 获取调用方IP地址
        String clientIP = RpcContext.getContext().getRemoteHost();

        // 获取当前服务配置信息，所有配置信息都将转换为URL的参数
        String application = RpcContext.getContext().getUrl().getParameter("application");

        System.out.println("isProviderSide=>"+isProviderSide);

        System.out.println("clientIP=>"+clientIP);

        System.out.println("application=>"+application);

        //TODO
        // 注意：每发起RPC调用，上下文状态会变化
        // 此时本端变成消费端，这里会返回false
        //boolean isProviderSide = RpcContext.getContext().isProviderSide();

        return books;
    }
}
