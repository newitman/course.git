package com.muke.dubbocourse.context.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 上下文信息 提供者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class ProviderXmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context/provider/spring/dubbo-provider-xml.xml");

        context.start();

        System.in.read();

    }

}
