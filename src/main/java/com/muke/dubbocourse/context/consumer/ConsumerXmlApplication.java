package com.muke.dubbocourse.context.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 上下文信息 消费者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class ConsumerXmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        context.refresh();

        print(RpcContext.getContext());

        BookFacade bookFacade = context.getBean(BookFacade.class);

        System.out.println("Result=>"+bookFacade.queryAll());

        print(RpcContext.getContext());


        System.in.read();

        //context.close();

    }

    private static void print(RpcContext context){

        try {
            // 本端是否为消费端，这里会返回true
            boolean isConsumerSide = context.isConsumerSide();

            // 获取最后一次调用的提供方IP地址
            String serverIP = context.getRemoteHost();

            // 获取当前服务配置信息，所有配置信息都将转换为URL的参数
            String application = RpcContext.getContext().getUrl().getParameter("application");

            // 注意：每发起RPC调用，上下文状态会变化
            //yyyService.yyy();

            System.out.println("isConsumerSide=>"+isConsumerSide);

            System.out.println("serverIP=>"+serverIP);

            System.out.println("application=>"+application);

        }catch (Exception e){


        }

    }

}
