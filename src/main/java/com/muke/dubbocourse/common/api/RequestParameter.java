package com.muke.dubbocourse.common.api;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className RequestParameter
 * @description
 * @JunitTest: {@link  }
 * @date 2020-11-04 22:07
 **/
public class RequestParameter implements Serializable {

    @NotEmpty(message = "查询书名称不能为空")
    @Size(min = 1, max = 10)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RequestParameter{" +
                "name='" + name + '\'' +
                '}';
    }
}
