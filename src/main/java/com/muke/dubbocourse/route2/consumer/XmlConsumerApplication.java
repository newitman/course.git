package com.muke.dubbocourse.route2.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 标签路由
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlConsumerApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("route2/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        BookFacade bookFacade = context.getBean(BookFacade.class);

        RequestParameter requestParameter = new RequestParameter();

        requestParameter.setName("SpringBoot");

//        RpcContext.getContext().setAttachment(CommonConstants.TAG_KEY,"TAG_A");
        RpcContext.getContext().setAttachment(CommonConstants.TAG_KEY,"TAG_B");

        List<Book> books = bookFacade.queryByName(requestParameter);

        System.out.println("Result2=> "+ books);

        System.in.read();

        //context.close();

    }

}
