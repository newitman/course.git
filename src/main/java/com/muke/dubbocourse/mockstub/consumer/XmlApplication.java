package com.muke.dubbocourse.mockstub.consumer;

import com.muke.dubbocourse.mockstub.api.BookFacade;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description Mock/Stub 服务消费者
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mockstub/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        BookFacade bookFacade = context.getBean(BookFacade.class);

        System.out.println("Result=>"+bookFacade.queryAll());

        System.in.read();

        //context.close();

    }

}
