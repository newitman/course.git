package com.muke.dubbocourse.mockstub.api;

import com.google.common.collect.Lists;
import com.muke.dubbocourse.common.domain.Book;

import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookFacadeStub
 * @description
 * @JunitTest: {@link  }
 * @date 2020-11-15 23:43
 **/
public class BookFacadeStub implements BookFacade {

    private BookFacade bookFacade;

    public BookFacadeStub(BookFacade bookFacade){

        this.bookFacade = bookFacade;

    }

    @Override
    public List<Book> queryAll() {

        try {

            //做一些前置处理
            System.out.println("调用方法queryAll前置处理");
            return bookFacade.queryAll();

        } catch (Exception e) {
            // 发生异常做一些处理
            return Lists.newArrayList();

        }finally {

            //做一些后置处理
            System.out.println("调用方法queryAll后置处理");
        }

    }

}
