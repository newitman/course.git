package com.muke.dubbocourse.mockstub.api;

import com.google.common.collect.Lists;
import com.muke.dubbocourse.common.domain.Book;

import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className BookFacadeMock
 * @description
 * @JunitTest: {@link  }
 * @date 2020-11-15 23:49
 **/
public class BookFacadeMock implements BookFacade {

    /**
     *
     * 这里我们可以把服务端的方法执行时间加大 使之超时就可以触发Mock的调用
     *
     * @author liyong
     * @date 12:18 AM 2020/11/16
     * @param
     * @exception
     * @return List<Book>
     **/
    @Override
    public List<Book> queryAll() {

        // 你可以伪造容错数据，此方法只在出现RpcException时被执行
        Book book = new Book();
        book.setDesc("default");
        book.setName("default");
        return Lists.newArrayList(book);

    }

}
