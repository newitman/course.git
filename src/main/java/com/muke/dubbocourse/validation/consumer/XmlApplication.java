package com.muke.dubbocourse.validation.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.api.RequestParameter;
import com.muke.dubbocourse.common.domain.Book;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 参数校验 消费端
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("validation/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        BookFacade bookFacade = context.getBean(BookFacade.class);

        RequestParameter request = new RequestParameter();

//        request.setName("SpringBoot");

        List<Book> books = bookFacade.queryByName(null);

        System.out.println("Result=>"+books);

        System.in.read();

        //context.close();

    }

}
