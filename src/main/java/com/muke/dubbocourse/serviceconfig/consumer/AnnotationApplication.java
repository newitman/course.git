package com.muke.dubbocourse.serviceconfig.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className AnnotationApplication
 * @description 注解配置
 * @JunitTest: {@link  }
 * @date 2020-10-24 23:08
 **/

@EnableDubbo(scanBasePackages = "com.muke.dubbocourse.serviceconfig.provider")
@PropertySource("classpath:/serviceconfig/consumer/spring/dubbo-consumer.properties")
@ComponentScan(value = {"com.muke.dubbocourse.serviceconfig.consumer"})
public class AnnotationApplication {

    public static void main(String[] args){

        // 创建 BeanFactory 容器
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        // 注册 Configuration Class（配置类） -> Spring Bean
        applicationContext.register(AnnotationApplication.class);

        // 启动 Spring 应用上下文
        applicationContext.refresh();

        BookFacade bookFacade = applicationContext.getBean(BookFacade.class);

        System.out.println("AnnotationApplication Result is =>" + bookFacade.queryAll());

        applicationContext.close();

    }
}
