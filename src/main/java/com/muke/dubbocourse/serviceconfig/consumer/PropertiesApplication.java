package com.muke.dubbocourse.serviceconfig.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 属性配置方式
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class PropertiesApplication {

    public static void main(String[] args) {

        System.setProperty("dubbo.properties.file","/Users/liyong/work/workspace/course/src/main/resources/serviceconfig/dubbo.properties");

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("serviceconfig/consumer/spring/dubbo-consumer-properties.xml");

        context.start();

        BookFacade bookFacade = context.getBean("bookFacade", BookFacade.class);

        System.out.println("Result=>"+ bookFacade.queryAll());

    }

}
