package com.muke.dubbocourse.serviceconfig.consumer;

import com.muke.dubbocourse.common.api.BookFacade;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description XML配置服务
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class XmlApplication {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("serviceconfig/consumer/spring/dubbo-consumer-xml.xml");

        context.start();

        BookFacade bookFacade = context.getBean("bookFacade", BookFacade.class);

        System.out.println("Result=>"+ bookFacade.queryAll());

    }

}
