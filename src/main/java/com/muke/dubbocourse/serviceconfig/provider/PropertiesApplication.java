package com.muke.dubbocourse.serviceconfig.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className Application
 * @description 属性配置服务
 * @JunitTest: {@link  }
 * @date 2020-10-24 21:49
 **/
public class PropertiesApplication {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("serviceconfig/provider/spring/dubbo-provider-properties.xml");

        context.start();

        System.in.read();

    }

}
