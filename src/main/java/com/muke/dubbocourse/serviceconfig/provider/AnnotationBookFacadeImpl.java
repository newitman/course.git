package com.muke.dubbocourse.serviceconfig.provider;

import com.muke.dubbocourse.common.api.BookFacade;
import com.muke.dubbocourse.common.domain.Book;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className AnnotationBookFacadeImpl
 * @description Dubbo注解使用
 * @JunitTest: {@link  }
 * @date 2020-10-24 23:01
 **/
@DubboService
public class AnnotationBookFacadeImpl implements BookFacade {

    private static final List<Book> books = new ArrayList<>();

    static {

        Book book1 = new Book();
        book1.setName("SpringBoot");
        book1.setDesc("SpringBoot实战");

        Book book2 = new Book();
        book2.setName("Spring");
        book2.setDesc("Spring实战");

        books.add(book1);

        books.add(book2);


    }

    @Override
    public List<Book> queryAll() {
        return books;
    }
}
