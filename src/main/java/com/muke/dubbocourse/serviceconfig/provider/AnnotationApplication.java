package com.muke.dubbocourse.serviceconfig.provider;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className AnnotationApplication
 * @description
 * @JunitTest: {@link  }
 * @date 2020-10-24 23:06
 **/
@EnableDubbo(scanBasePackages = "com.muke.dubbocourse.serviceconfig.provider")
@PropertySource("classpath:/serviceconfig/provider/spring/dubbo-provider.properties")
public class AnnotationApplication {

    public static void main(String[] args) throws IOException {

        // 创建 BeanFactory 容器
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        // 注册 Configuration Class（配置类） -> Spring Bean
        applicationContext.register(AnnotationApplication.class);

        // 启动 Spring 应用上下文
        applicationContext.refresh();

        System.in.read();

    }
}
