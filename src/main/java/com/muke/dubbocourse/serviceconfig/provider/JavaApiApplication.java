package com.muke.dubbocourse.serviceconfig.provider;

import com.muke.dubbocourse.common.api.BookFacade;
import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ProtocolConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.ServiceConfig;

import java.io.IOException;

/**
 * @author <a href="http://youngitman.tech">青年IT男</a>
 * @version v1.0.0
 * @className JavaApiApplication
 * @description API配置方式
 * @JunitTest: {@link  }
 * @date 2020-10-24 22:46
 **/
public class JavaApiApplication {

    public static void main(String[] args) throws IOException {

        export();

        //使服务Hold住
        System.in.read();

    }

    /**
     *
     * 暴露服务
     *
     * @author liyong
     * @date 10:49 PM 2020/10/24
     * @param
     * @exception
     * @return void
     **/
    private static void export() {

        BookFacadeImpl bookFacade = new BookFacadeImpl();

        // 当前应用配置
        ApplicationConfig application = new ApplicationConfig();
        application.setName("course");

        // 连接注册中心配置
        RegistryConfig registry = new RegistryConfig();
        registry.setAddress("zookeeper://127.0.0.1:2181");
        registry.setUsername("aaa");
        registry.setPassword("bbb");

        // 服务提供者协议配置
        ProtocolConfig protocol = new ProtocolConfig();
        protocol.setName("dubbo");
        protocol.setPort(12345);
        protocol.setThreads(200);

        // 注意：ServiceConfig为重对象，内部封装了与注册中心的连接，以及开启服务端口

        // 服务提供者暴露服务配置
        ServiceConfig<BookFacade> service = new ServiceConfig<>(); // 此实例很重，封装了与注册中心的连接，请自行缓存，否则可能造成内存和连接泄漏
        service.setApplication(application);
        service.setRegistry(registry); // 多个注册中心可以用setRegistries()
        service.setProtocol(protocol); // 多个协议可以用setProtocols()
        service.setInterface(BookFacade.class);
        service.setRef(bookFacade);
        service.setVersion("1.0.0");

        // 暴露及注册服务
        service.export();

    }

}
